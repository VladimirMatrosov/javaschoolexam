package com.tsystems.javaschool.tasks.calculator;

public class Operators {
    public static final char ADDITION = '+';
    public static final char SUBSTRACTION = '-';
    public static final char DIVISION = '/';
    public static final char MULTIPLICATION = '*';
    public static final char OPEN_BRACKET = '(';
    public static final char CLOSE_BRACKET = ')';
    public static final char SEPARATOR = '.';

    public static boolean isOperator(char input) {
        boolean out = false;
        switch (input) {
            case ADDITION:
                out = true;
            case SUBSTRACTION:
                out = true;
            case DIVISION:
                out = true;
            case MULTIPLICATION:
                out = true;
            case OPEN_BRACKET:
                out = true;
            case CLOSE_BRACKET:
                out = true;
            case SEPARATOR:
                out = true;
        }

        return out;
    }

    public static boolean isNumber(char input) {
        boolean out = false;
        try {
            String str = String.valueOf(input);
            int inputNum = Integer.parseInt(str);
            switch (inputNum) {
                case 1:
                    out = true;
                case 2:
                    out = true;
                case 3:
                    out = true;
                case 4:
                    out = true;
                case 5:
                    out = true;
                case 6:
                    out = true;
                case 7:
                    out = true;
                case 8:
                    out = true;
                case 9:
                    out = true;
                case 0:
                    out = true;
            }
            return out;
        } catch (Exception ex){
            return false;
        }
    }

    public static boolean isSeparator(char input) {
        if (input == SEPARATOR)
            return true;
        else
            return false;
    }

    public static boolean isOpenBracket(char input) {
        if (input == OPEN_BRACKET)
            return true;
        else
            return false;
    }

    public static boolean isCloseBracket(char input) {
        if (input == CLOSE_BRACKET)
            return true;
        else
            return false;
    }

    public static boolean isDivisionOrMultiplication(char input) {
        if ((input == DIVISION) || (input == MULTIPLICATION))
            return true;
        else
            return false;
    }
}
