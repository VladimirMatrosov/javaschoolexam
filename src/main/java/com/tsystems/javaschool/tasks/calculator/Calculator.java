package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import static com.tsystems.javaschool.tasks.calculator.Operators.*;

public class Calculator {
    private List output;
    private List<Character> operators;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            String result = "";
            this.parseToOPN(statement);
            this.parseOPN();
            Double res = Double.parseDouble(output.get(0).toString());
            for (int i = 1; i < output.size(); i++) {
                for (int j = 0; j < operators.size(); j++) {
                    switch (operators.get(j)) {
                        case ADDITION:
                            res += Double.parseDouble(output.get(i).toString());
                            break;
                        case SUBSTRACTION:
                            res -= Double.parseDouble(output.get(i).toString());
                            break;
                    }
                }
            }
            result = String.format("%.4f", res);
            String string = result;
            for (int i = 1; i <= 4; i++) {
                char test = string.charAt(string.length() - i);
                if (test == '0') {
                    result = result.substring(0, result.length() - 1);
                } else
                    break;
            }

            if (result.charAt(result.length() - 1) == ',')
                result = result.substring(0, result.length() - 1);
            return result;
        } catch (NullPointerException exception) {
            return null;
        }
    }

    private List parseOPN() {
        int i = 0;
        List<Double> result = new ArrayList();
        String subject = "";
        while (i < output.size()) {
            String charIn = output.get(i).toString();
            if (isNumber(charIn.charAt(0))) {
                if (i < output.size() - 1) {
                    String charNum = output.get(i + 1).toString();
                    if (isSeparator(charNum.charAt(0))) {
                        subject = charIn + charNum + output.get(i + 2);
                        i += 2;
                        result.add(Double.parseDouble(subject));
                    } else
                        result.add(Double.parseDouble(charIn));
                } else
                    result.add(Double.parseDouble(charIn));
            } else if (isOperator(charIn.charAt(0))) {
                switch (charIn.charAt(0)) {
                    case ADDITION:
                        result.set(result.size() - 1, result.get(result.size() - 1) +
                                Double.parseDouble(output.get(i + 1).toString()));
                        break;
                    case SUBSTRACTION:
                        result.set(result.size() - 1, result.get(result.size() - 1) -
                                Double.parseDouble(output.get(i + 1).toString()));
                        break;
                    case DIVISION:
                        result.set(result.size() - 1, result.get(result.size() - 1) /
                                Double.parseDouble(output.get(i + 1).toString()));
                        break;
                    case MULTIPLICATION:
                        result.set(result.size() - 1, result.get(result.size() - 1) *
                                Double.parseDouble(output.get(i + 1).toString()));
                        break;
                }
                i++;
            } else
                return null;
            i++;
        }
        output = result;
        return output;
    }


    private List parseToOPN(String input) {
        output = new ArrayList();
        operators = new ArrayList<>();
        input = input.replaceAll("\\s", "");
        char[] inputChars = input.toCharArray();
        int i = 0;
        String number = "";
        while (i < inputChars.length) {
            if (isOperator(inputChars[i])) {
                if (isSeparator(inputChars[i])) {
                    if (isSeparator(inputChars[i + 1]))
                        return null;
                    else if ((output.isEmpty()) && (number.equals("")))
                        return null;
                    else {
                        if (!number.equals("")) {
                            number += ".";
                        } else
                            return null;
                    }
                } else {
                    if (!number.equals("")) {
                        output.add(number);
                        number = "";
                    }
                }
                if (isOpenBracket(inputChars[i])) {
                    operators.add(inputChars[i]);
                } else if (isCloseBracket(inputChars[i])) {
                    if (isOpenBracket(operators.get(operators.size() - 1))) {
                        operators.remove(operators.size() - 1);
                    } else
                        return null;
                } else if (isDivisionOrMultiplication(inputChars[i]))
                    output.add(inputChars[i]);
                else {
                    if ((operators.size() != 0) && (isOpenBracket(operators.get(operators.size() - 1))))
                        output.add(inputChars[i]);
                    else if (!isSeparator(inputChars[i]))
                        operators.add(inputChars[i]);

                }
            } else if (isNumber(inputChars[i])) {
                number += inputChars[i];
            } else return null;
            i++;
            if ((!number.equals("")) && (i == inputChars.length)) {
                output.add(number);
                number = "";
            }
        }
        List result = new ArrayList();
        result.add(output);
        result.add(operators);
        return result;
    }

}
